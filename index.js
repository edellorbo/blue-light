require('log-timestamp');
var events = require('events');
events.defaultMaxListeners = 20; //fix a bug leaking listeners within 'yeelight-blue'

process.on('uncaughtException', function(e) {
   console.log("BlueLight will restart now.");
   console.log('An error has occured. error is: %s and stack trace is: %s', e, e.stack);
   process.exit(1);
});
require('throw-max-listeners-error');
var YB = require('yeelight-blue');
var host = "0.0.0.0";
var port = 2222;
var express = require("express");
var light, discoInterval;
var disco = [
    [248, 12, 18],
    [238, 17, 0],
    [255, 51, 17],
    [255, 68, 34],
    [255, 102, 68],
    [255, 153, 51],
    [254, 174, 45],
    [204, 187, 51],
    [208, 195, 16],
    [170, 204, 34],
    [105, 208, 37],
    [34, 204, 170],
    [18, 189, 185],
    [17, 170, 187],
    [68, 68, 221],
    [51, 17, 187],
    [59, 12, 189],
    [68, 34, 153]
];
var colors = {
  red : [255,0,0],
  normal : [255,214,200],
  white  : [255,255,255],
  green  : [100,247,175],
  violet : [225,186,243],
  salmon : [245,192,152],
  pantone: [136,176,75]
};
var lastColor = colors.normal;
var lastDim = 100;

var setLight = function(params){
  console.log('setting light to state:',params);
  if(light)  light.setColorAndBrightness.apply(light,lastColor.concat([lastDim]));
}
var hexToRgb = function(str){
  var colorParser = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
  var hexToDec = x => parseInt(x,16);
  return colorParser.exec(str).slice(1,4).map(hexToDec);
}
function onDeviceFound(yeelightBlue) {
  console.log('found: ' + yeelightBlue.uuid);

  yeelightBlue.connectAndSetup(function(){

    console.log('light connected: ' +  yeelightBlue.uuid);

    light = yeelightBlue;

    var fs = require("fs");
    var app = express();

    app.use(express.static(__dirname + "/public")); //use static files in ROOT/public folder
    app.use(function(req,res,next){
        if(discoInterval){
            clearInterval(discoInterval)
            discoInterval = null;
            light.setGradualMode(false, function(){});
        }
        next();
    })

    app.get("/light/dim/more", function(request, response){
        if(lastDim < 100) lastDim +=25;
        setLight(lastColor.concat([lastDim]));
        response.send('change dim to: ' +lastDim );
    });
    app.get("/light/dim/all", function(request, response){
        lastDim = 100;
        setLight(lastColor.concat([lastDim]));
        response.send('change dim to: ' +lastDim );
    });
    app.get("/light/dim/none", function(request, response){
        lastDim=0;
        setLight(lastColor.concat([lastDim]));
        response.send('change dim to: ' +lastDim );
    });
    app.get("/light/dim/toggle", function(request, response){
        lastDim = lastDim > 0 ? 0 :100;
        setLight(lastColor.concat([lastDim]));
        response.send('toggle dim to: ' +lastDim );
    });
    app.get("/light/dim/less", function(request, response){
        if(lastDim >0) lastDim -=25;
        setLight(lastColor.concat([lastDim]));
        response.send('change dim to: ' + lastDim)
    });
    app.get("/light/dim/sunset",(request, response) => {
      light.setGradualMode(true, ()=>{});
      var i = 100;
      setTimeout(function me(){
          if(i < 0 ) {
            return light.setColorAndBrightness.apply(light, [80,59,24,0]);
          }
          setTimeout(me,1000);
	         console.log('set light ',i);
          light.setColorAndBrightness.apply(light, [80,59,24,i]);
          i-=10;
      },10);
      response.send('change to: ' +request.params.color);
    });
    app.get("/light/disco", function(request, response){
        light.setGradualMode(true, function(){});
        var i = 0;
        discoInterval = setInterval(function(){
            if(i > disco.length -1 ) i = 0;
            var nextColor = disco[i];
            light.setColorAndBrightness.apply(light,nextColor.concat([100]));
            i++;
        },1000)
        response.send('change to: ' +request.params.color);
    });
    app.get("/light/hex/:hex", function(request, response){
        console.log(request.params.hex,hexToRgb(request.params.hex))
        lastColor = hexToRgb('#'+request.params.hex);
        setLight(lastColor.concat([lastDim]));
        response.send('change to:' + request.params.hex);
    });
    app.get("/light/color/:color", function(request, response){ //root dir
        lastColor = colors[request.params.color];
        setLight(lastColor.concat([lastDim]));
        response.send('change to: ' +request.params.color);
    });


    app.listen(port, host);

  });
}
console.log('starting blue-light ...');
YB.discover(onDeviceFound);
