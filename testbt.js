var YB = require('yeelight-blue');
var action = process.argv[2];
var times = parseInt(process.argv[3]) > 0 ? parseInt(process.argv[3])  : Infinity;
var colors = {
  red : [255,0,0,100],
  white : [255,255,255,100],
  green : [111,255,0,100],
  off : [255,255,255,0]
};
if(!(action in colors)){
    console.log('unknown command');
    process.exit();
}
console.log('searching light');

function blinkColor(bulb,color,fn){
  bulb.setColorAndBrightness.apply(bulb,
    color.concat([function(){
      console.log('set light to ' + action);
      setTimeout(function(){
        bulb.setColorAndBrightness.apply(bulb,colors.off.concat([fn]));
          setTimeout(function(){ blinkColor(bulb,color,fn) },1e3)
      },1e3)
    }])
  );
}
YB.discover(function(bulb) {
  console.log('hey found: ' + bulb.uuid);
  bulb.connectAndSetup(function(){
    console.log('connected');
    var i = 0;
    blinkColor(bulb, colors[action]);

  });
});
